/**
 * Admin
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

  schema: true,

  attributes: {
    username: {
      type: 'string',
      required: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      delete obj.confirmation;
      return obj;
    }
 
  },

  beforeCreate: function(values, next) {
    var bcrypt = require('bcrypt');
    if (!values.password || values.password != values.confirmation) {
      return next({err : ["Password doesn't match password confirmation"]});
    }
    bcrypt.hash(values.password, 10, function(err, hash) {
      if (err) return next(err);
      values.password = hash;
      next();
    });
  }

};
