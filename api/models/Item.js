/**
 * Item
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

  schema: true,

  attributes: {
    
    food_id: {
      type: 'string',
      required: true
    },

    survey_id: {
      type: 'string',
      required: true
    },

    derivation: {
      type: 'string',
      required: true
    },

    name: {
      type: 'string',
      required: true
    },

    description: {
      type: 'string',
      required: true
    },

    details: {
      type: 'string',
      required: true
    },

    energy: {
      type: 'string',
      required: true
    },

    food0: {
      type: 'string'
    },

    percentage0: {
      type: 'float'
    },

    food1: {
      type: 'string'
    },

    percentage1: {
      type: 'float'
    },

    food2: {
      type: 'string'
    },

    percentage2: {
      type: 'float'
    },

    food3: {
      type: 'string'
    },

    percentage3: {
      type: 'float'
    },

    food4: {
      type: 'string'
    },

    percentage4: {
      type: 'float'
    },

    food5: {
      type: 'string'
    },

    percentage5: {
      type: 'float'
    },

    food6: {
      type: 'string'
    },

    percentage6: {
      type: 'float'
    },

    food7: {
      type: 'string'
    },

    percentage7: {
      type: 'float'
    },

    food8: {
      type: 'string'
    },

    percentage8: {
      type: 'float'
    },

    food9: {
      type: 'string'
    },

    percentage9: {
      type: 'float'
    }


  }
};
