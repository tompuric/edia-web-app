/**
 * DailyLog
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

  schema: true,

  attributes: {
    
    email: {
      type: 'string',
      required: true,
      email: true
    },

    survey_id: {
      type: 'string',
      required: true
    },

    quantity: {
      type: 'string',
      required: true
    },

    time_eaten: {
      type: 'string',
      required: true
    },

    location: {
      type: 'string',
      required: true
    },

    meal_type: {
      type: 'string',
      required: true
    },

    /*
      Might need to add columns for each food type (vege/meat/fruit...)
    */

  }

};

//SELECT * FROM statistics WHERE date BETWEEN datetime('now', ' -1 days ') AND datetime('now', 'start of day')