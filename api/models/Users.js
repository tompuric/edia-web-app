/**
 * Users
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

  schema: true,

  attributes: {
    full_name: {
      type: 'string',
      required: true,
    },

    email: {
      type: 'string',
      required: true,
      email: true,
      unique: true
    },

    gender: {
      type: 'string',
      required: true
    },

    weight: {
      type: 'float',
      required: true
    },

    height: {
      type: 'integer',
      required: true
    },

    dob: {
      type: 'string',
      required: true
    },

    password: {
      type: 'string',
      required: true
    },

    activity: {
      type: 'string',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      delete obj.confirmation;
      return obj;
    }
 
  },

  beforeCreate: function(values, next) {
    var bcrypt = require('bcrypt');
    if (!values.password || values.password != values.confirmation) {
      return next({err : ["Password doesn't match password confirmation"]});
    }
    bcrypt.hash(values.password, 10, function(err, hash) {
      if (err) return next(err);
      values.password = hash;
      next();
    });
  }

};
