/**
 * Nutrient
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

  attributes: {
  	
  	food_id: {
      type: 'string',
      required: true,
    },

    survey_id: {
      type: 'string',
      required: true,
    },

    name: {
      type: 'string',
      required: true,
    },

    moisture: {
      type: 'string',
      required: true,
    },

    energy_with_fibre: {
      type: 'string',
      required: true,
    },

    energy_without_fibre: {
      type: 'string',
      required: true,
    },

    protein: {
      type: 'string',
      required: true,
    },

    fat: {
      type: 'string',
      required: true,
    },

    carbs: {
      type: 'string',
      required: true,
    },

    sugar: {
      type: 'string',
      required: true,
    },

    starch: {
      type: 'string',
      required: true,
    },

    fibre: {
      type: 'string',
      required: true,
    },

    alcohol: {
      type: 'string',
      required: true,
    },

    sat_fat_acid: {
      type: 'string',
      required: true,
    },

    mono_fat_acid: {
      type: 'string',
      required: true,
    },

    poly_fat_acid: {
      type: 'string',
      required: true,
    },

    lin_acid: {
      type: 'string',
      required: true,
    },

    alpha_lin_acid: {
      type: 'string',
      required: true,
    },

    omega: {
      type: 'string',
      required: true,
    },

    vita_a: {
      type: 'string',
      required: true,
    },

    pre_vita_a: {
      type: 'string',
      required: true,
    },

    pro_vita_a: {
      type: 'string',
      required: true,
    },

    thiamin: {
      type: 'string',
      required: true,
    },

    riboflavin: {
      type: 'string',
      required: true,
    },

    niacin: {
      type: 'string',
      required: true,
    },

    folate1: {
      type: 'string',
      required: true,
    },

    folate2: {
      type: 'string',
      required: true,
    },

    vita_c: {
      type: 'string',
      required: true,
    },

    vita_d: {
      type: 'string',
      required: true,
    },

    vita_e: {
      type: 'string',
      required: true,
    },

    calcium: {
      type: 'string',
      required: true,
    },

    iron: {
      type: 'string',
      required: true,
    },

    iodine: {
      type: 'string',
      required: true,
    },

    magnesium: {
      type: 'string',
      required: true,
    },

    phosphorus: {
      type: 'string',
      required: true,
    },

    potassium: {
      type: 'string',
      required: true,
    },

    sodium: {
      type: 'string',
      required: true,
    },

    zinc: {
      type: 'string',
      required: true,
    },

    caffeine: {
      type: 'string',
      required: true,
    },

    cholesterol: {
      type: 'string',
      required: true,
    }
    
  }

};
