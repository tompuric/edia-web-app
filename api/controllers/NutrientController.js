/**
 * NutrientController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

module.exports = {

  /* e.g.
  sayHello: function (req, res) {
    res.send('hello world!');
  }
  */
  //var schemaKeyList = ['', ''],
  create: function (req, res, next) {
  	Nutrient.create(req.params.all(), function nutrientCreated(err, user) {
		if (err) {
			console.log("failed");
			return;
		}
		res.json(user);
  	});
  }
  

};
