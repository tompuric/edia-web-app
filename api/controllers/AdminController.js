/**
 * AdminController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

module.exports = {

  /* e.g.
  sayHello: function (req, res) {
    res.send('hello world!');
  }
  */

  login: function(req, res, next) {
    var username = req.param("username");
    var password = req.param("password");

    var bcrypt = require('bcrypt');
      Admin.findOne({'username': username}, function foundAdmin(err, user) {
        if (err) {
          console.log("errors reading database");
          return res.send(202, "Invalid Input");
        }
        if (!user) {
          console.log("Invalid Username");
          return res.send('/');
        }
        var hash = bcrypt.compareSync(password, user.password);
        if (hash) {

          req.session.authenticated = true;
          req.session.Admin = user;

          res.redirect('/users');
          return;
        }
        else {
          console.log("Invalid Password");
          res.redirect('/');
        }
      });
  }
  

};
