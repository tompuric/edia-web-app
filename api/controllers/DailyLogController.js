/**
 * DailyLogController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

module.exports = {

  /* e.g.
  sayHello: function (req, res) {
    res.send('hello world!');
  }
  */
  log: function(req, res, next) {
    DailyLog.create(req.params.all(), function (err, log) {
      if (err) {
        console.log(err);
        res.send(err);
      }
      else {
        var user = req.param('email');
        var food = req.param('survey_id');
        console.log("success, user: %s, id: %s", user, food);
        res.send(203, "Successful Logging");
      }
    });
  },

  


};
