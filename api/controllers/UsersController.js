/**
 * UsersController
 *
 * @module    :: Controller
 * @description :: Contains logic for handling requests.
 */

module.exports = {


  /*
    Registers users for the android app
    @param name, email, password, confirmation, gender,
      weight, height, dob, activity_level
    @throw Database Error, Email Exists
    @return a registered User in the database
  */
  register: function (req, res) {
    // extracts the variables from the request to check them from the db
    var email = req.param("email");
    var password = req.param("password");

    // Finds if the email entered is in the database
    Users.findByEmail(email).done(function (err, email) {
      if (err) {
        console.log("error: Database Error");
        res.send(500, {error: "Database Error"});
      }
      // If it exists, reject the request
      else if (Object.keys(email).length) {
        console.log("error: Email Exists");
        res.send(201, "Email Exists");
      }
      // If not, create the new user
      else {
        Users.create(req.params.all(), function (err, user) {
          if (err) {
            console.log(err);
            res.send(202, "Invalid Input");
          }
          else {
            console.log("success: " + req.params.toString());
            res.send(203, "Successful Registration");
          }
        });
      }

    });
  },

  /*
    Logins Users for the android app. Enables users to log in if they've already made an account.
    This function checks the current database to see if email and passwords match.
    @param email, password
    @throw next if email/password is incorrect
    @return a response that says the user is logged in
  */
  login: function(req, res, next) {
    // extracts the variables from the request
    var email = req.param("email");
    var password = req.param("password");

    // Gets the encryption program to encrypt passwords
    var bcrypt = require('bcrypt');
      Users.findOne({'email': email}, function foundUser(err, user) {
        if (err) {
          console.log("errors yo");
          return res.send(202, "Invalid Input");
        }
        // Checks if the username is incorrect
        if (!user) {
          console.log("Invalid Username");
          return res.send(201, "Invalid Username");
        }
        // Encrypts the password and compares with stored
        var hash = bcrypt.compareSync(password, user.password );
        // If they are the same, user gets logged in
        if (hash) {
          console.log("Logged In");
          res.send(203, "Logged in");
        }
        // If not, user is rejected
        else {
          console.log("Invalid Password");
          res.send(201, "Invalid Password");
        }
      });
  },

  /*
    starts HEI algorithm based on date given
  */
  dateHEI: function(req, res) {

  },


  /*
    Health Index provides the healthy eating score index of a persons daily dietary intake.
    It's based off the Sydney Universities algorithms for determining the HEI score of a person.
    @param user, date
    @throw database error
    @return HEI score of user for specific date
  */
  healthIndex: function(req, res) {
    // Initial variables set up for serving sizes of different food groups
    // The following are measured in kJ. Using 'Mean Energy Serve'
    var fruitServe = 350;
    var vegeServe = 225;
    var leguServe = 225;
    var meatServe = 550;
    var dairyServe = 550;
    var grainServe = 500;
    var discServe = 600;
    var sugarServe = 200;
    // The following is measured in Grams
    var fatServe = 5;

    // extracts the variables from the request
    var userId = req.param('id');
    var date = req.param('date');
    var dateStatus = req.param('status');

    // Loads the Q module for extensive asynchronous functions
    var Q = require('q');

    // Prints to console the start of the HEI algorithm
    console.log("healthIndex");

    // Collects all the food items eaten for a specific date
    // (being one day in total)
    function findLogs(user) {
      // Initilises deferred variable to store function data
      var deferred = Q.defer();

      // Gets the specific date and the day after
      var d1 = new Date(date);
      var d2 = new Date(date);
      // Get date wanted and adjust date value accordingly
      if (dateStatus == "pre") {
        d1.setDate(d1.getDate() - 1);
        d2.setDate(d2.getDate() - 1);
      }
      if (dateStatus == "post") {
        d1.setDate(d1.getDate() + 1);
        d2.setDate(d2.getDate() + 1);
      }
      date = d1.format('yyyy-mm-dd');
      // split dates 1 day apart
      d2.setDate(d1.getDate() + 1);
      try {
        // corrects the format for the specified date and the day after
        var st1 = d1.format('yyyy-mm-dd');
        var st2 = d2.format('yyyy-mm-dd');
      }
      catch (err) {
        return err;
      }

      // Locates all the meals within that day by that particular user
      DailyLog.find()
      .where({email: user.email})
      .where({'time_eaten': {'>=': st1, '<': st2}})
      .sort('survey_id')
      .exec( function (err, logs) {
        deferred.resolve({"logs": logs, "user": user});
      })

      // Returns all the food items eaten on that date
      return deferred.promise;
    }

    /*
      getTypes starts to get the required values of all nutritional component groupings
      for each of the log items and stores this data into a list
      Such types are all the HEI Food Codes and their associated percentages
    */
    function getTypes(p) {
      // Extracts the users logs from the parameter
      var logs = p.logs;

      // Create list for the survey_id, energy, grams, and type for each log
      var list = [];

      // Go through each log to extract the required information
      for (var i = 0; i < logs.length; i++) {
        // Gets the Item data (values of all nutritional component groupings) for each log
        Item.findOne({'survey_id': logs[i].survey_id}, function foundItem(err, item) {
          if (err) 
            console.log("error");
          else {
            if (item == undefined)
              // If this is returned, it meals the user has entered an invalid food not recognised
              return console.log("caught");

            // Inits dictionary as the types are a combination of any of the 30+ types. We've limited
            // the current number of HEI Food Codes for a particular item to be 10. (This can expand)
            var types = {};

            // The types here are each of the 10 possible food types and their percentage values
            // The type and value is stored in the 'types' dictionary
            types[item.food0] = item.percentage0;
            types[item.food1] = item.percentage1;
            types[item.food2] = item.percentage2;
            types[item.food3] = item.percentage3;
            types[item.food4] = item.percentage4;
            types[item.food5] = item.percentage5;
            types[item.food6] = item.percentage6;
            types[item.food7] = item.percentage7;
            types[item.food8] = item.percentage8;
            types[item.food9] = item.percentage9;

            // Stores the items survey_id, the energy in it, the total grams consumed, and
            // the dictionary of HEI Food Codes and their percentages
            list.push({'survey_id': item.survey_id,
                       'energy': item.energy,
                       'grams': logs[i].quantity,
                       'type': types});
          }
        });

      }
      // Returns the user, their eaten items (logs), and the data from the previous function
      return {'user': p.user, 'logs': logs, 'data': list};
    }

    /*
      Display logs retrieves the data from its parameter and returns all the required
      data necessary for the web page to display all the data for the user. This data
      includes the individual HEI Score Components, their total score, and total consumed
      kJ for each category.
    */
    function displayLogs(p) {
      // Prints parameter to console to ensure all data is there
      //console.log(p);

      // Calculates total grams consumed for all foods
      var grams = 0;
      for (var i = p.logs.length - 1; i >= 0; i--) {
        grams += parseInt(p.logs[i].quantity);
      };

      // Displays web page with required data
      console.log(date);
      res.view('users/show', {
        'method': req.route.method,
        'hei': p.hei,
        'user': p.user,
        'logs': p.logs, 
        'energy': p.hei.saturatedFat.energy,
        'grams': grams,
        'badmeals': p.badmeals,
        'vegetables': p.vegetables,
        'fruits': p.fruits,
        'grains': p.grains,
        'meats': p.meats,
        'dairy': p.dairy,
        'water': p.water,
        'saturatedFat': p.saturatedFat,
        'sodium': p.sodium,
        'emptyCal': p.emptyCal,
        'alcohol': p.alcohol,
        'score': p.score,
        'date': date
      });
    }

    // Checks if a value from a key in a dictionary is undefined.
    // If so, return 0, otherwise return the value
    function propertyKey(key, val) {
      if (key[val] == undefined)
        return 0;
      else 
        return key[val];
    }

    /* 1
      Measure of unhealthy choices, Extra foods: frequency of consumption of extra foods per day.
      Gets the total percentage of kJ from each food item containing discretionary food codes
    */
    function getMeals(data) {
      // HEI Food Codes for disretionary foods
      var meals = ["DIS", "MP", "OIL", "FAT", "AS"];
      var mealsVal = 0.0;

      // Per food Item
      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;

        // Get the discretionary values. If item = 1, assume all of it is a discretionary food item and add it's total kJ
        if (propertyKey(groups, meals[0]) == 1) {
          mealsVal += (data[i].grams/100) * data[i].energy;
          continue;
        }
        // Per food group needed. Get the other food groups
        for (var j = 0; j < meals.length - 1; j++) {
          // sums the kJ for the specific food group from the food item
          mealsVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, meals[j]);
        }
      }
      return mealsVal;

    }

    /*
      do1 is the getMeals add on. It calculates the number of serves based on kJ and
      whether or not the user is female/male
    */
    function do1(g, val) {
      var serves = val/discServe;
      var s = 3;
      // if nothing eaten, return 0.
      if (val == 0) return 0;
      if (g == "female") s = 2.5;
      if (serves < s) return 10;
      else if (serves >= s && serves < s + 1) return 7.5
      else if (serves >= s + 1 && serves < s + 2) return 5;
      else if (serves >= s + 2 && serves < s + 3) return 2.5;
      else return 0;
    } 

    /* 2
      Gets the total percentage of kJ from each food item containing vegetable food codes
    */
    function getVegetables(data) {
      // HEI Food Codes for vegetables
      var veges = ["V", "VO", "VG", "VC", "VTB", "L"];

      var vegeVal = 0.0;
      var VO = 0.0; // orange
      var VG = 0.0; // green
      var VC = 0.0; // cruciferous
      var VTB = 0.0; // tuber/bulb
      var L = 0.0; // Legume
      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;discServe
        for (var j = 0; j < veges.length - 1; j++) {
          vegeVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, veges[j]);
          if (veges[j] == "VO") VO += data[i].grams * propertyKey(groups, veges[j]);
          if (veges[j] == "VG") VG += data[i].grams * propertyKey(groups, veges[j]);
          if (veges[j] == "VC") VC += data[i].grams * propertyKey(groups, veges[j]);
          if (veges[j] == "VTB") VTB += data[i].grams * propertyKey(groups, veges[j]);
          if (veges[j] == "L") L += data[i].grams * propertyKey(groups, veges[j]);
        }
      }
      return {"vegeVal": vegeVal, "VO": VO, "VG": VG, "VC": VC, "VTB": VTB, "L": L};
    }

    /*
      do2 is the getVegetables add on. It calculates the number of serves based on kJ and
      whether or not the user is female/male. It also adds more values depending on the
      number of different types of vegetables eaten
    */
    function do2(g, val) {
      var serves = val.vegeVal/vegeServe;
      var result = 0.0;
      if (g == "male") {
        if (serves >= 6) result += 5;
        else if (serves >= 4.8 && serves < 6) result += 4;
        else if (serves >= 3.6 && serves < 4.8) result += 3;
        else if (serves >= 2.4 && serves < 3.6) result += 2;
        else if (serves >= 1.0 && serves < 2.4) result += 1;
        else result += 0;
      }
      else {
        if (serves >= 5) result += 5;
        else if (serves >= 4 && serves < 5) result += 4;
        else if (serves >= 3 && serves < 4) result += 3;
        else if (serves >= 2 && serves < 3) result += 2;
        else if (serves >= 1 && serves < 2) result += 1;
        else result += 0;
      }

      if (val.VO/vegeServe >= 1) result++;
      if (val.VG/vegeServe >= 1) result++;
      if (val.VC/vegeServe >= 1) result++;
      if (val.VTB/vegeServe >= 1) result++;
      if (val.L/vegeServe >= 1) result++;

      return result;

    }

    /* 3
      Gets the total percentage of kJ and number of different fruit types 
      from each food item containing fruit food codes
    */
    function getFruits(data) {
      // Identifies the variety of fruits consumed
      var variety = [];

      // HEI Food Codes for fruits
      var fruits = ["FRT", "FRTB", "FRTP", "FRTC", "FRTS", "FRTT"];
      var fruitsVal = 0.0;

      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;
        for (var j = 0; j < fruits.length - 1; j++) {
          fruitsVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, fruits[j]);
          if (propertyKey(groups, fruits[j]) != 0)
            variety[fruits[i]] = 1;
        }
        
      }
      return {'fruitsVal': fruitsVal, 'fruitsNum': Object.keys(variety).length};
    }

    /*
      do3 is the getFruits add on. It calculates the number of serves based on kJ and
      how many different fruits were eaten
    */
    function do3(g, val) {
      var serves = val.fruitsVal/fruitServe;
      var result = 0.0;
      if (serves >= 2) result += 5;
      else if (serves >= 1.5 && serves < 2) result += 3.75;
      else if (serves >= 1 && serves < 1.5) result += 2.5;
      else if (serves >= 0.5 && serves < 1) result += 1.25;
      else result += 0;

      if (val.fruitsNum >= 2) results += 5;
      
      return result;
      
    }

    /* 4
      Gets the total percentage of kJ from each food item containing grain food codes and splits
      the data between grains and wholegrains
    */
    function getGrains(data) {
      // HEI Food Codes for Grains
      var grains = ["G", "WG"];
      var grainsVal = 0.0;
      var wholeVal = 0.0
      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;
        for (var j = 0; j < grains.length - 1; j++) {
          grainsVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, grains[j]);
          if (grains[i] == "WG")
            wholeVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, grains[j]);
        }
      }
      return {'grainsVal': grainsVal, 'wholeVal': wholeVal};
    }

    /*
      do4 is the getGrains add on. It calculates the number of serves based on kJ from both whole grains
      and normal grains
    */
    function do4(g, val) {
      var result = 0.0;
      var whole = val.wholeVal/grainServe;
      var serves = val.grainsVal/grainServe;
      if (serves >= 6) result += 5;
      else if (serves >= 5 && serves < 6) result += 4.17;
      else if (serves >= 4 && serves < 5) result += 3.34;
      else if (serves >= 3 && serves < 4) result += 2.5;
      else if (serves >= 2 && serves < 3) result += 1.67;
      else if (serves >= 1 && serves < 2) result += 0.84;
      else result += 0;

      if (whole >= 3) result += 5;
      else if (whole >= 1 && whole < 3) result += 2.5;
      else result += 0;

      return result;
    }

    /* 5
      Gets the total percentage of kJ from each food item containing meat food codes and splits
      the values based on whether they are meats, fish, and processed meats.
    */
    function getMeats(data) {
      // HEI Food Codes for meats
      var meats = ["MA", "MP", "MR", "FSH", "SF", "EGG", "L", "SDS", "NT"];
      var meatsVal = 0.0;
      var fishVal = 0.0;
      var redVal = 0.0;
      var proVal = 0;
      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;
        for (var j = 0; j < meats.length - 1; j++) {
          if (meats[j] == "MP" && propertyKey(groups, meats[j]) != 0) proVal++;
          if (meats[j] == "FSH") fishVal += data[i].grams * propertyKey(groups, meats[j]);
          if (meats[j] == "MR") redVal += data[i].grams * propertyKey(groups, meats[j])
          meatsVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, meats[j]);
        }
      }
      return {'fish': fishVal, 'red': redVal, 'meatsVal': meatsVal};
    }

    /*
      do5 is the getMeats add on. It calculates the number of serves based on kJ and
      whether or not the user is female/male. It also adds more depending on the number of
      red meat and fish consumed
    */
    function do5(g, val) {
      var serves = val.meatsVal/meatServe;
      var total = 0.0;

      if (g == "male") {
        if (serves >= 3) total += 4;
        else total += 0;
      }
      else {
        if (serves >= 2.5) total += 4;
        else total += 0;
      }

      if (val.red > 0 && val.red <= 65) total += 2;
      if (val.fish >= 30) total += 2;

      return total;

    }

    /* 6
      Gets the total percentage of kJ from each food item containing dairy food codes
    */
    function getDairy(data) {
      // HEI Food Codes for dairy
      var dairy = ["D", "DA", "SOY"];
      var dairyVal = 0.0;
      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;
        for (var j = 0; j < dairy.length - 1; j++)
          dairyVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, dairy[j]);
      }
      return dairyVal;
    }

    /*
      do6 is the getDairy add on. It calculates the number of serves based on kJ
    */
    function do6(g, val) {
      var serves = val/dairyServe;
      if (serves >= 2.5) return 10;
      else if (serves >= 2 && serves < 2.5) return 8;
      else if (serves >= 1.5 && serves < 2.5) return 6;
      else if (serves >= 1 && serves < 1.5) return 4;
      else if (serves >= 0.5 && serves < 1) return 2;
      else return 0;
    }

    /* 7
      Gets the total percentage of grams from each food item containing water food codes
      as well as the number of beverages consumed throughout the day
    */
    function getWater(data) {
      // HEI Food Codes for water
      var water = ["F", "W"];
      var waterVal = 0.0;
      var bevs = 0;
      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;
        for (var j = 0; j < water.length - 1; j++) {
          if (propertyKey(groups, water[j]) != 0) {
            bevs++;
            waterVal += propertyKey(groups, water[j]);
          }
        }
      }
      return {'grams': waterVal, 'beverages': bevs};
    }

    /*
      do7 is the getWater add on. It calculates the number of serves based on grams over
      number of beverages consumed
    */
    function do7(g, val) {
      var percentage = val.grams/val.beverages;
      if (percentage >= 50) return 5;
      else if (percentage >= 40 && percentage < 50) return 4;
      else if (percentage >= 30 && percentage < 40) return 3;
      else if (percentage >= 20 && percentage < 30) return 2;
      else if (percentage >= 10 && percentage < 20) return 1;
      else return 0;
    }

    /* 8
      Gets the total percentage of grams from each food item containing saturated fats.
      These fats are split into regular saturated fat and poly/mono saturated fats
    */
    function getSaturatedFat(data) {
      var energy = 0;
      var satVal = 0.0;
      var polymonoVal = 0.0;
      for (var i = 0; i < data.length; i++) {
        Nutrient.findOne({'survey_id': data[i].survey_id}, function (err, nutrient) {
          if (nutrient == undefined)
            return 0;
          else {
            satVal += parseFloat(nutrient.sat_fat_acid) * data[i].grams/100;
            polymonoVal +=  (parseFloat(nutrient.mono_fat_acid) + parseFloat(nutrient.poly_fat_acid)) * data[i].grams/100
            energy += parseInt(nutrient.energy_with_fibre) * parseInt(data[i].grams)/100;
          }
        })
      }
      return {'satFat': satVal, 'polymonoVal': polymonoVal, 'energy': energy};
    }

    // Need to segregate between margarine and oils (not sure if needs to change)
    /*
      do8 is the getSaturatedFat add on. It calculates the number of serves based on grams
      over total energy consumption and segregates them between margine and oils. Also depending on users sex
    */
    function do8(g, val) {
      // total saturated fat consumed
      var output = (val.satVal*37)/val.energy*100;
      var result = 0;
      if (output <= 10) result = 5;
      else if (output > 10 && output <= 12) result = 2.5;
      else result = 0;

      var serves = val.polymonoVal/fatServe;
      if (g == "male") {
        if (serves >= 4) result += 5;
        else if (serves >= 3 && serves < 4) result += 3.75;
        else if (serves >= 2 && serves < 3) result += 2.5;
        else if (serves >= 1 && serves < 2) result += 1.25;
        else result += 0;
      }
      else {
        if (serves >= 2) result += 5;
        else if (serves >= 1.5 && serves < 2) result += 3.75;
        else if (serves >= 1 && serves < 1.5) result += 2.5;
        else if (serves >= 0.5 && serves < 1) result += 1.25;
        else result += 0;
      }
      return result;
    }

    /* 9
      Gets the total percentage of mg from each food item containing sodium
    */
    function getSodium(data) {
      var sodiumVal = 0.0;
      
      for (var i = 0; i < data.length; i++) {

        Nutrient.findOne({'survey_id': data[i].survey_id}, function (err, nutrient) {
          if (nutrient == undefined)
            return 0;
          else
            sodiumVal += parseFloat(nutrient.sodium) * data[i].grams/100;
        });
      }
      return sodiumVal;
    }

    /*
      do9 is the getSodium add on. It calculates the number of serves based on mmol
    */
    function do9(g, val) {
      var mmol = val/23;
      // If no sodium is had, add nothing.
      if (mmol > 0 && mmol <= 70) return 10;
      else if (mmol > 70 && mmol <= 100) return 5;
      else return 0;
    }

    /* 10
      Gets the total percentage of kJ from each food item containing empty calorie food codes
      such as added sugar
    */
    function getEmptyCals(data) {
      // HEI Food Codes for empty calories (added sugar)
      var sugar = ["AS"];
      var sugarVal = 0.0;
      for (var i = 0; i < data.length; i++) {
        var groups = data[i].type;
        for (var j = 0; j < sugar.length - 1; j++)
          sugarVal += (data[i].grams/100) * data[i].energy * propertyKey(groups, sugar[j]);
      }
      return sugarVal;
    }

    /*
      do10 is the getEmptyCals add on. It calculates the number of serves based on kJ
    */
    function do10(g, val) {
      var serves = val/sugarServe;
      if (serves > 0 && serves <= 1.5) return 10;
      else if (serves > 1.5 && serves <= 2.0) return 6.7;
      else if (serves >= 2.0 && serves <= 3.0) return 3.3;
      else return 0;
    }

    /* 11
      Gets the total percentage of mg from each food item containing alcohol
    */
    function getAlcohol(data) {
      var alcoVal = 0.0;
      for (var i = 0; i < data.length; i++) {
        Nutrient.findOne({'survey_id': data[i].survey_id}, function (err, nutrient) {
          if (nutrient == undefined)
            return 0;
          else {
            alcoVal += parseFloat(nutrient.alcohol) * data[i].grams/100;
          }
        });
      }
      return alcoVal;
    }

    /*
      do11 is the getAlcohol add on. It calculates the number of serves based on grams
    */
    function do11(g, val) {
      var serves = val/10;
      if (serves > 0 && serves <= 2) return 5;
      else return 0;
    }

    /*
      Calculates and aggregates all the individual HEI components and their 
      associated scores/data and passes all the results to 'getHEIscore' function
    */
    function getHEIdata(data) {
      return Q.all([getMeals(data.data),
          getVegetables(data.data),
          getFruits(data.data),
          getGrains(data.data),
          getMeats(data.data),
          getDairy(data.data),
          getWater(data.data),
          getSaturatedFat(data.data),
          getSodium(data.data),
          getEmptyCals(data.data),
          getAlcohol(data.data)])
      .spread(function (b, v, f, g, m, d, w, sf, s, ec, a) {
        return {'user': data.user,
                'logs': data.logs,
                'hei': {
                  'badmeals': b,
                  'vegetables': v,
                  'fruits': f,
                  'grains': g,
                  'meats': m,
                  'dairy': d,
                  'water': w,
                  'saturatedFat': sf,
                  'sodium': s,
                  'emptyCal': ec,
                  'alcohol': a
                }
              };
      });
    }
    
    /*
      Determines if user is male or female and calculates the score of each HEI component.
      Then performs any last minute calculations needed for the final result from the
      aggregated data (For instance, a sum of all the total scores for the HEI Total Score)
      and sends it to the 'displayLogs' function.
    */
    function getHEIscore(data) {
      // Determine gender of user
      var g = data.user.gender;
      if (g !== "male" || g !== "female")
        g = "male"
      // Calculate the scores depending on the HEI data for each category (grouped by HEI Food Codes)
      return Q.all([do1(g, data.hei.badmeals),
          do2(g, data.hei.vegetables),
          do3(g, data.hei.fruits),
          do4(g, data.hei.grains),
          do5(g, data.hei.meats),
          do6(g, data.hei.dairy),
          do7(g, data.hei.water),
          do8(g, data.hei.saturatedFat),
          do9(g, data.hei.sodium),
          do10(g, data.hei.emptyCal),
          do11(g, data.hei.alcohol)])
      .spread(function (b, v, f, g, m, d, w, sf, s, ec, a) {
        // Sum the HEI Total Score
        var total = b + v + f + g + m + d + w + sf + s + ec + a;
        // Return all required data for the 'displayLogs' function
        return {'user': data.user,
                'logs': data.logs,
                'hei': data.hei,
                'badmeals': b,
                'vegetables': v,
                'fruits': f,
                'grains': g,
                'meats': m,
                'dairy': d,
                'water': w,
                'saturatedFat': sf,
                'sodium': s,
                'emptyCal': ec,
                'alcohol': a,
                'score': total};
      });
    }

    /*
      Peforms the asynchronous functions to calculate the HEI score.
      This shows the order of execution/flow of the functions above
    */

    Users.findOne(userId)
    .then(findLogs)
    .then(getTypes)
    .then(getHEIdata)
    .then(getHEIscore)
    .then(displayLogs)
    .fail(function (err) {
      console.log("error handled");
      console.log(err);
    })
    .fin( function (fin) {
      console.log("Finished");
    });

  },


  /*
    Show an individuals users statistics
    @param 
    @throw 
    @return
  */
  show: function(req, res) {
    var userId = req.param('id');
    var Q = require('q');

    function findLogs(user) {
      var deferred = Q.defer();
      var d1 = new Date();
      d1.setDate(d1.getDate() + 1);
      var st1 = d1.format('yyyy-mm-dd');

      DailyLog.find()
      .where({email: user.email})
      .where({'time_eaten': {'<=': st1}})
      .sort('time_eaten')
      .exec( function (err, logs) {
        deferred.resolve({"logs": logs, "user": user});
      })

      return deferred.promise;
    }

    /*
      Displays all the eaten food items (logs) that the user ate for their
      entire existence
    */
    function displayLogs(p) {
      var grams = 0;
      for (var i = p.logs.length - 1; i >= 0; i--) {
        grams += parseInt(p.logs[i].quantity);
      };
      res.view({
        'method': req.route.method,
        'user': p.user,
        'logs': p.logs, 
        'grams': grams,
        'meals': grams/50
      });
    }

    /*
      Finds an individual user basd on their userId
    */
    Users.findOne(userId)
    .then(findLogs)
    .then(displayLogs)
    .fail(function (err) {
      console.log("Error caught");
      res.send(202, "Error occurred");
    })
    .fin( function (fin) {
      console.log("Finished");
    });
  },

  // View all users. Gets the list of users
  index: function(req, res, next) {
    Users.find(function foundUsers (err, users) {
      if (err) return next(err);
      res.view({
        users: users
      });
    });
  },

  /*
    Edit. Edits a users' data
    @param users id
    @throw user does not exist
    @return an edited user
  */
  edit: function(req, res, next) {
    Users.findOne(req.param('id'), function foundUser (err, user) {
      if (err) return next(err);
      if (!user) return next();
      res.view({
        users: user
      });
    });
  },

  /*
    Update. Updates a users' data
    @param users id
    @throw user does not exist
    @return an updated user
  */
  update: function(req, res, next) {
    Users.update(req.param('id'), req.params.all(), function foundUser (err, user) {
      if (err) 
        res.redirect('/users/edit/' + req.param('id'));
      res.redirect('/users/show/' + req.param('id'));
    });
  },

  /*
    Delete. Removes a user from the system
    @param users id
    @throw User doesn't exist
    @return a removed user
  */
  delete: function(req, res, next) {
    Users.findOne(req.param('id'), function userDestroyed (err, user) {
      if (err) return next(err);
      if (!user) return next('User doesn\'t exist.');
      User.destroy(req.param('id'), function userDestroyed(err) {
        if (err) return next(err);
      });
      res.redirect('/users');
    });
  }
};



//--------------------------------------------------



/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
  var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
    timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
    timezoneClip = /[^-+\dA-Z]/g,
    pad = function (val, len) {
      val = String(val);
      len = len || 2;
      while (val.length < len) val = "0" + val;
      return val;
    };

  // Regexes and supporting functions are cached through closure
  return function (date, mask, utc) {
    var dF = dateFormat;

    // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
    if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
      mask = date;
      date = undefined;
    }

    // Passing date through Date applies Date.parse, if necessary
    date = date ? new Date(date) : new Date;
    if (isNaN(date)) throw SyntaxError("invalid date");

    mask = String(dF.masks[mask] || mask || dF.masks["default"]);

    // Allow setting the utc argument via the mask
    if (mask.slice(0, 4) == "UTC:") {
      mask = mask.slice(4);
      utc = true;
    }

    var _ = utc ? "getUTC" : "get",
      d = date[_ + "Date"](),
      D = date[_ + "Day"](),
      m = date[_ + "Month"](),
      y = date[_ + "FullYear"](),
      H = date[_ + "Hours"](),
      M = date[_ + "Minutes"](),
      s = date[_ + "Seconds"](),
      L = date[_ + "Milliseconds"](),
      o = utc ? 0 : date.getTimezoneOffset(),
      flags = {
        d:    d,
        dd:   pad(d),
        ddd:  dF.i18n.dayNames[D],
        dddd: dF.i18n.dayNames[D + 7],
        m:    m + 1,
        mm:   pad(m + 1),
        mmm:  dF.i18n.monthNames[m],
        mmmm: dF.i18n.monthNames[m + 12],
        yy:   String(y).slice(2),
        yyyy: y,
        h:    H % 12 || 12,
        hh:   pad(H % 12 || 12),
        H:    H,
        HH:   pad(H),
        M:    M,
        MM:   pad(M),
        s:    s,
        ss:   pad(s),
        l:    pad(L, 3),
        L:    pad(L > 99 ? Math.round(L / 10) : L),
        t:    H < 12 ? "a"  : "p",
        tt:   H < 12 ? "am" : "pm",
        T:    H < 12 ? "A"  : "P",
        TT:   H < 12 ? "AM" : "PM",
        Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
        o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
        S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
      };

    return mask.replace(token, function ($0) {
      return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
    });
  };
}();

// Some common format strings
dateFormat.masks = {
  "default":      "ddd mmm dd yyyy HH:MM:ss",
  shortDate:      "m/d/yy",
  mediumDate:     "mmm d, yyyy",
  longDate:       "mmmm d, yyyy",
  fullDate:       "dddd, mmmm d, yyyy",
  shortTime:      "h:MM TT",
  mediumTime:     "h:MM:ss TT",
  longTime:       "h:MM:ss TT Z",
  isoDate:        "yyyy-mm-dd",
  isoTime:        "HH:MM:ss",
  isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
  isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
  dayNames: [
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
    "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
  ],
  monthNames: [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
  ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
  return dateFormat(this, mask, utc);
};
