/**
 * ItemController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

module.exports = {

  /* e.g.
  sayHello: function (req, res) {
    res.send('hello world!');
  }
  */

  create: function (req, res, next) {
  	console.log(req.params.all());
  	Item.create(req.params.all(), function itemCreated(err, user) {
		if (err) {
			console.log(err);
			return res.send(404);
		}
		res.json(user);
  	});
  },

  removeItems: function(req, res, next) {
    Item.destroy({}).done(function(err) {
      if (err) console.log("err");
      console.log("items removed");
    });
  }
  

};
