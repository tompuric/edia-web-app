JSHINT=./node_modules/.bin/jshint
LINTCONFIG=./config/lint.json

.PHONY: install lint

all: install lint

install:
		npm install

lint:
		${JSHINT} api/models/*.js --config ${LINTCONFIG}
		${JSHINT} api/policies/*.js --config ${LINTCONFIG}
		${JSHINT} api/controllers/*.js --config ${LINTCONFIG}
